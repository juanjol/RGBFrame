/*
  Morse.h - Library for flashing Morse code.
  Created by David A. Mellis, November 2, 2007.
  Released into the public domain.
*/
#ifndef SpritesLibrary_h
#define SpritesLibrary_h

#include "Arduino.h"

class SpritesLibrary
{
  public:
    SpritesLibrary();
    void getAnimation(char name[], long *animation, int timer);
    void getFlorMario(long *sprites);

  private:
    int _pin;
};

#endif